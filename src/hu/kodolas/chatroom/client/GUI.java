package hu.kodolas.chatroom.client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.Collection;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class GUI extends JPanel {

    public static final Color PANEL_BG = new Color(200, 200, 200);
    ChatClient chatClient;

    public JFrame frame;                    // Az ablak
    public JButton sendButton, shakeButton; // A küldés és a ShakeWindow gomb
    public JTextArea inputMsg;              // A szövegdoboz, amibe az üzenetet lehet írni
    public JTextArea messages;              // Az üzeneteket mutató szövegdoboz
    public JTextArea onlineUsers;           // Az online felhasználókat mutató szövegdoboz

    
    /*
     * A felhasználói felületet a Swing és az AWT könyvtárak
     * segítségével jelenítjük meg.
     *
     * A panel GridBagLayout elrendezést használ, amivel
     * rácsszerűen lehet elrendezni a komponenseket.
     *
     * A hivatalos dokumentáció:
     * https://docs.oracle.com/javase/tutorial/uiswing/components/index.html
     */
    public GUI(JFrame frame, ChatClient chatClient) {
        this.chatClient = chatClient;
        this.frame = frame;
        
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.insets = new Insets(5, 5, 5, 5);

        // Az üzenetek szövegdoboza
        gbc.gridx = 0;
        gbc.gridy = 0;
        messages = new JTextArea();
        messages.setEditable(false);
        JScrollPane messagesScroll = new JScrollPane(messages);
        messagesScroll.setPreferredSize(new Dimension(500, 350));
        messagesScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(messagesScroll, gbc);

        // Az online felhasználók szövegdoboza
        gbc.gridx = 1;
        gbc.gridy = 0;
        JPanel panel2 = new JPanel();
        panel2.setBackground(PANEL_BG);
        panel2.setPreferredSize(new Dimension(100, 350));
        onlineUsers = new JTextArea();
        onlineUsers.setBackground(PANEL_BG);
        onlineUsers.setEditable(false);
        panel2.add(onlineUsers);
        add(panel2, gbc);

        // A szerkeszthető szövegdoboz, amibe a felhasználó írhatja az üzenetet
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridheight = 2;
        inputMsg = new JTextArea(4, 20);
        inputMsg.setLineWrap(true);
        JScrollPane inputScroll = new JScrollPane(inputMsg);
        inputScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        add(inputScroll, gbc);

        // A Küldés gomb
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridheight = 1;
        sendButton = new JButton("Küldés");
        sendButton.addActionListener(new Listener());
        add(sendButton, gbc);

        // A Shake!! gomb
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridheight = 1;
        shakeButton = new JButton("Shake!!");
        shakeButton.addActionListener(new Listener());
        add(shakeButton, gbc);
    }

    // Ha megváltozik az online felhasználók listája, frissítjük a szövegdoboz
    // tartalmát
    void onlineUsersChanged(Collection<String> users) {
        onlineUsers.setText(""); // Az eddigi szöveg törlése
        for (String username : users) { // Az összes felhasználónév hozzáadása a szövegdobozhoz
            onlineUsers.append(username + "\n");
        }
    }

    /*
     * Ha érkezik egy új üzenet, hozzáadjuk a szövegdobozhoz a feladó nevével 
     * együtt.
     * A String formázásról bővebben:
     * https://dzone.com/articles/java-string-format-examples
     */
    void newMessageReceived(String sender, String msg) {
        messages.append(String.format("[%s]: %s", sender, msg) + "\n");
    }
    
    // A shake window animáció elindítása
    void shakeWindow() {
        new ShakeWindowAnimation().start();
    }
    
    /*
     * A Listener egy belső osztály, ami az ActionListener
     * kiterjesztése. Az actionPerformed() függvény meg
     * fog hívódni, ha rákattint a felhasználó valamelyik gombra.
     * Bővebben: https://docs.oracle.com/javase/tutorial/uiswing/events/actionlistener.html
     */
    private class Listener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                if (e.getSource() == sendButton) { // ha a küldés gombot nyomta meg a felhasználó
                    chatClient.sendMessage(inputMsg.getText());
                    inputMsg.setText("");
                } else { // különben a ShakeWindow! gombot nyomta meg a felhasználó
                    chatClient.sendShakeWindow();
                }
            } catch (IOException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private class ShakeWindowAnimation extends Thread {
        
        /*
         * Ez az osztály kezeli a shake window animációt.
         * A Thread osztály kiterjesztése, ezért a run()
         * függvényben lévő kód egy külön szálon fog
         * futni, hogy ne zavarjon be a ChatClient
         * hálózati hallgatózásába.
         */
        
        /*
         * Az animáció abból áll, hogy az ablakot elmozdítjuk rövid idő
         * alatt egy véletlenszerű vektorral, majd vissza mozdítjuk a
         * kezdő pozícióba az ellentétes irányú vektorral. Ezt a
         * műveletet megismételjük párszor.
         */
        
        // Ha elindítjuk a szálat, akkor a run() függvény fog meghívódni
        @Override
        public void run() {
            Random random = new Random();
            try {
                for (int i = 0; i < 5; i++) {
                    /*
                     * A random.nextInt(n) egy véletlenszerű számot generál
                     * 0 és n között, a 0-t beleértve, de n-et nem.
                     */
                    
                    // Az x és y irányú eltolás (pixelben)
                    int dx = random.nextInt(41) - 20; // A random szám generálás a [-20;20] zárt intervallumban
                    int dy = random.nextInt(41) - 20;
                    moveWindow(5, dx, dy);   // az ablak elmozdítása a (dx;dy) vektorral, 5 képernyőfrissítés alatt
                    moveWindow(5, -dx, -dy); // az ablak elmozdítása az ellentétes, -(dx;dy) = (-dx;-dy) vektorral
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        /*
         * Ez a függvény animálva mozdítja el az ablakot a megadott
         * vektorral a megadott idő alatt.
         *
         * Az idő itt valójában a képernyőfrissítések (frame-ek) számát
         * jelenti. Azaz ha 5-tel hívjuk meg, az azt jelenti, hogy
         * ötször állítjuk be az ablak pozícióját, az eltelt idő alapján.
         * A tényleges időt akkor kapjuk meg, ha megszorozzuk a 
         * képernyőfrissítések számát azzal az idővel, hogy mennyi
         * idő telik el két frissítés között.
        
         * A frame egy állapotot jelent, jelen esetben egy pozícióját
         * az ablaknak. Viszont ne tévesszen meg, hogy az ablak
         * változóját is frame-nek hívják a JFrame osztály miatt.
         */
        void moveWindow(int time, double dX, double dY) throws InterruptedException {  
            int startX = (int) frame.getLocationOnScreen().getX(); // eltároljuk az ablak eredeti pozícióját
            int startY = (int) frame.getLocationOnScreen().getY();

            for (int i = 1; i <= time; i++) { // az összes frame-et megjelenítjük.
                double proportion = (double) i / (double) time; // az eddig megjelenített frame-ek és az összes frame aránya
                int x = startX + (int) (dX * proportion); // ha megszorozzuk az arányt a koordinátákkal, és hozzáadjuk
                int y = startY + (int) (dY * proportion); // az eredeti koordinátákhoz, megkapjuk az új koordnátákat.
                frame.setLocation(x, y); // az ablak áthelyezése az új koordinátákra 
                Thread.sleep(10L); // 10 ezredmásodpercig várakoztatjuk a szálat, azaz
                                   // ennyi ideig lesz látható egy frame.
                                   // Egy másodperc alatt tehát 100 képernyőfrissítés
                                   // lesz. 
            }
        }

    }
}
